import torch
from torch import nn
import torch.nn.functional as F
from models.superpoint import SuperPoint
from models.superglue import SuperGlue, normalize_keypoints
from models.matching import Matching


class Output(nn.Module):
    def __init__(self):
        super(Output, self).__init__()
        self.fc1 = nn.Linear(4000, 8192)
        self.bn1 = nn.BatchNorm1d(8192) 
        
        self.fc2 = nn.Linear(8192, 4096)
        self.bn2 = nn.BatchNorm1d(4096) 
        
        self.fc3 = nn.Linear(4096, 2048)
        self.bn3 = nn.BatchNorm1d(2048) 

        self.fc4 = nn.Linear(2048, 1024)
        self.bn4 = nn.BatchNorm1d(1024) 

        self.fc5 = nn.Linear(1024, 512)
        self.bn5 = nn.BatchNorm1d(512) 

        self.fc6 = nn.Linear(512, 256)
        self.bn6 = nn.BatchNorm1d(256) 
        
        self.fc7 = nn.Linear(256, 128)
        self.bn7 = nn.BatchNorm1d(128) 

        self.fc8 = nn.Linear(128, 64)
        self.bn8 = nn.BatchNorm1d(64) 

        self.fc9 = nn.Linear(64, 32)
        self.bn9 = nn.BatchNorm1d(32) 

        self.fc10 = nn.Linear(32, 16)
        self.bn10 = nn.BatchNorm1d(16) 

        self.fc11 = nn.Linear(16, 6)
        self.relu = nn.ReLU()

    def forward(self, kpts0, kpts1, matches0, matches1, scores0, scores1):
        matches0 = torch.unsqueeze(matches0, -1)
        #matches1 = torch.unsqueeze(matches1, -1)
        scores0 = torch.unsqueeze(scores0, -1)
        #scores1 = torch.unsqueeze(scores1, -1)
        #x = torch.cat((kpts0, kpts1, matches0, matches1, scores0, scores1), 2)
        x = torch.cat((kpts0, kpts1, matches0, scores0), 2)

        x = x.view(-1, 4000)
        x = self.fc1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.bn2(x)
        x = self.relu(x)

        x = self.fc3(x)
        x = self.bn3(x)
        x = self.relu(x)

        x = self.fc4(x)
        x = self.bn4(x)
        x = self.relu(x)

        x = self.fc5(x)
        x = self.bn5(x)
        x = self.relu(x)

        x = self.fc6(x)
        x = self.bn6(x)
        x = self.relu(x)
        
        x = self.fc7(x)
        x = self.bn7(x)
        x = self.relu(x)

        x = self.fc8(x)
        x = self.bn8(x)
        x = self.relu(x)

        x = self.fc9(x)
        x = self.bn9(x)
        x = self.relu(x)
        
        x = self.fc10(x)
        x = self.bn10(x)
        x = self.relu(x)

        x = self.fc11(x)
        return x


def append_depth_to_kpts(all_kpts, depths):
    
    depths_batch = []
    batch = 0
    for kpts in all_kpts:
        ds = []
        for pos in kpts:
            ds.append(depths[batch, 0, pos[1].long(), pos[0].long()])
        depths_batch.append(torch.cat([torch.unsqueeze(d, 0) for d in ds]))
        batch+=1
    depths = torch.cat([torch.unsqueeze(d, 0) for d in depths_batch])
    depths = torch.unsqueeze(depths, -1)
    return torch.cat((all_kpts, depths), dim=-1)
    

class SimpleModel(nn.Module):
    def __init__(self):
        super(SimpleModel, self).__init__()
        self.config = {
                        'superpoint': {
                            'nms_radius': 4,
                            'keypoint_threshold': 0.005,
                            'max_keypoints': 500
                        },
                        'superglue': {
                            'weights': 'outdoor',
                            'sinkhorn_iterations': 20,
                            'match_threshold': 0.2,
                        }
                    }

        self.superpoint = SuperPoint(self.config.get('superpoint', {}))

        for param in self.superpoint.parameters():
            param.requires_grad = False

        self.superglue = SuperGlue(self.config.get('superglue', {}))
        for param in self.superglue.parameters():
            param.requires_grad = False

        self.output = Output().train()
    
    def forward(self, inp0, inp1, d0, d1):
        pred = {}
        x0 = self.superpoint({'image': inp0})
        pred = {**pred, **{k+'0': v for k, v in x0.items()}}
        x1 = self.superpoint({'image': inp1})
        pred = {**pred, **{k+'1': v for k, v in x1.items()}}
        data = {**{'image0': inp0, 'image1': inp1}, **pred}

        for k in data:
            if isinstance(data[k], (list, tuple)):
                new_data = []
                if(k.startswith('keypoints')):
                    #padding keypoints
                    for kpt in data[k]:
                        new_data += [F.pad(kpt, 
                                    (0, 0, 0, self.config['superpoint']['max_keypoints'] - kpt.shape[0]))]

                if(k.startswith('descriptor')):
                    #padding descriptors
                    for desc in data[k]:
                        new_data += [F.pad(desc, 
                                    (0, self.config['superpoint']['max_keypoints'] - desc.shape[1]))]

                if(k.startswith('score')):
                    #padding scores
                    for score in data[k]:
                        new_data += [F.pad(score, 
                                    (0, self.config['superpoint']['max_keypoints'] - score.shape[0]))]
                data[k] = torch.stack(new_data)

        matches = self.superglue(data)

        kpts0 = normalize_keypoints(data['keypoints0'], 
                                    [0, 0, inp0.shape[-2], 
                                    inp0.shape[-1]])
        kpts0 = append_depth_to_kpts(kpts0, d0)

        matches0 = matches['matches0']/inp0.shape[-1]

        scores0 = matches['matching_scores0']

        kpts1 = normalize_keypoints(data['keypoints1'], 
                                    [0, 0, inp1.shape[-2], 
                                    inp1.shape[-1]])
        kpts1 = append_depth_to_kpts(kpts1, d1)

        matches1 = matches['matches1']/inp1.shape[-1]                       
        scores1 = matches['matching_scores1']
        x = self.output(kpts0, kpts1, matches0, matches1, scores0, scores1)
        return x


if __name__ == '__main__':
    import time
    import numpy as np
    from image_loader import get_iterator
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = SimpleModel()
    model.to(device)
    it = get_iterator('/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/', 
                    cycle_every=None, batch_size=2, max_kpts=500, sequences_names=['04'], max_skip=1, random_seed=42)
    
    for x, y in it.iterate():
        # (N, C, H, W)
        img0 = torch.from_numpy(x[0]/255).float().unsqueeze(1).to(device)
        img1 = torch.from_numpy(x[1]/255).float().unsqueeze(1).to(device)
        d0 = torch.from_numpy(x[2]/255).float().unsqueeze(1).to(device)
        d1 = torch.from_numpy(x[3]/255).float().unsqueeze(1).to(device)

        init = time.time()
        # A full forward pass
        x = model(img0, img1, d0, d1)
        #print("out ", x.shape)
        print(time.time() - init)
        break