import torch
from torch import nn
import torch.nn.functional as F
from models.utils import frame2tensor
from models.superpoint import SuperPoint
from models.superglue_modified import SuperGlue, normalize_keypoints
from models.matching import Matching
from model import OutputPose


class JointTrain(nn.Module):
    def __init__(self):
        super(JointTrain, self).__init__()
        self.config = {
                        'superpoint': {
                            'nms_radius': 4,
                            'keypoint_threshold': 0.005,
                            'max_keypoints': 500
                        },
                        'superglue': {
                            'weights': 'outdoor',
                            'sinkhorn_iterations': 20,
                            'match_threshold': 0.2,
                        }
                    }

        self.superpoint = SuperPoint(self.config.get('superpoint', {})).train()
        self.superglue = SuperGlue(self.config.get('superglue', {})).train()
        self.output = OutputPose().train()
    
    def forward(self, inp0, inp1):
        pred = {}
        x0 = self.superpoint({'image': inp0})
        pred = {**pred, **{k+'0': v for k, v in x0.items()}}
        x1 = self.superpoint({'image': inp1})
        pred = {**pred, **{k+'1': v for k, v in x1.items()}}
        data = {**{'image0': inp0, 'image1': inp1}, **pred}

        for k in data:
            if isinstance(data[k], (list, tuple)):
                new_data = []
                if(k.startswith('keypoints')):
                    #padding keypoints
                    for kpt in data[k]:
                        new_data += [F.pad(kpt, 
                                    (0, 0, 0, self.config['superpoint']['max_keypoints'] - kpt.shape[0]))]

                if(k.startswith('descriptor')):
                    #padding descriptors
                    for desc in data[k]:
                        new_data += [F.pad(desc, 
                                    (0, self.config['superpoint']['max_keypoints'] - desc.shape[1]))]

                if(k.startswith('score')):
                    #padding scores
                    for score in data[k]:
                        new_data += [F.pad(score, 
                                    (0, self.config['superpoint']['max_keypoints'] - score.shape[0]))]
                data[k] = torch.stack(new_data)
        
        matching_scores = self.superglue(data)
        kpts0 = normalize_keypoints(data['keypoints0'], 
                                    [0, 0, inp0.shape[-2], 
                                    inp0.shape[-1]])

        kpts1 = normalize_keypoints(data['keypoints1'], 
                                    [0, 0, inp1.shape[-2], 
                                    inp1.shape[-1]])
        x = self.output(kpts0, kpts1, matching_scores)

        return x



if __name__ == '__main__':
    import time
    import numpy as np
    from image_loader import get_iterator
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model = JointTrain()
    model.to(device)
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("data_path", default=None, help="Path to data")
    parser.add_argument("batch_size", default=2, help="batch size", type=int)
    args = parser.parse_args()

    it = get_iterator(args.data_path, #'/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/', 
                    cycle_every=None, batch_size=args.batch_size, max_kpts=500, sequences_names=['03'], max_skip=1, random_seed=42)
    
    for x, y in it.iterate():
        # (N, C, H, W)
        inp0 = torch.from_numpy(x[0]/255).float().unsqueeze(1).to(device)
        inp1 = torch.from_numpy(x[1]/255).float().unsqueeze(1).to(device)
        
        init = time.time()
        # A full forward pass
        x = model(inp0, inp1)
        #print("out ", x.shape)
        print(time.time() - init)
        #break