import os
import cv2
from copy import deepcopy
from pathlib import Path
import torch
from torch import nn
import torch.nn.functional as F
from dataset import get_iterator
import numpy as np
    
class ModelViz(nn.Module):
    def __init__(self, config={}):
        super(ModelViz, self).__init__()
        self.config = {**config}
        
        self.conv1 = nn.Conv1d(in_channels=500, out_channels=1024, kernel_size=3, padding=1)
        self.conv2 = nn.Conv1d(in_channels=1024, out_channels=512, kernel_size=3, padding=1)
        self.conv3 = nn.Conv1d(in_channels=512, out_channels=256, kernel_size=3, padding=1) 
        self.conv4 = nn.Conv1d(in_channels=256, out_channels=128, kernel_size=3, padding=1) 
        self.conv5 = nn.Conv1d(in_channels=128, out_channels=64, kernel_size=3, padding=1) 
        self.conv6 = nn.Conv1d(in_channels=64, out_channels=32, kernel_size=3, padding=1) 
        self.conv7 = nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, padding=1) 
        self.conv8 = nn.Conv1d(in_channels=16, out_channels=2, kernel_size=3, padding=1) 
        self.softmax = nn.Softmax(dim=2)
        
        self.fc1 = nn.Linear(500*4, 1024)
        self.bn1 = nn.BatchNorm1d(1024)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(1024, 512)
        self.bn2 = nn.BatchNorm1d(512)
        self.fc3 = nn.Linear(512, 256)
        self.bn3 = nn.BatchNorm1d(256)
        self.fc4 = nn.Linear(256, 128)
        self.bn4 = nn.BatchNorm1d(128) 
        self.fc5 = nn.Linear(128, 64)
        self.bn5 = nn.BatchNorm1d(64)        
        self.fc6 = nn.Linear(64, 32)
        self.bn6 = nn.BatchNorm1d(32) 
        self.out = nn.Linear(32, 6)

    def forward(self, kpts0, kpts1, matching_scores):
        soft = self.softmax(matching_scores)
        x = self.conv1(soft)
        x = self.relu(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.conv4(x)
        x = self.relu(x)
        x = self.conv5(x)
        x = self.relu(x)
        x = self.conv6(x)
        x = self.relu(x)
        x = self.conv7(x)
        x = self.relu(x)
        x = self.conv8(x)
        x = self.relu(x)

        w0 = x[:,0].unsqueeze(-1)
        w1 = x[:,1].unsqueeze(-1)

        kpts0 = kpts0 * w0
        kpts1 = kpts1 * w1

        x = torch.cat((kpts0, kpts1), 2)
        x = x.view(-1, 500*4)
        x = self.fc1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.bn2(x)
        x = self.relu(x)

        x = self.fc3(x)
        x = self.bn3(x)
        x = self.relu(x)

        x = self.fc4(x)
        x = self.bn4(x)
        x = self.relu(x)

        x = self.fc5(x)
        x = self.bn5(x)
        x = self.relu(x)

        x = self.fc6(x)
        x = self.bn6(x)
        x = self.relu(x)
        
        x = self.out(x)
        return x, soft

def load_model(path):
    model = ModelViz()
    model.load_state_dict(torch.load(path, map_location=lambda storage, loc: storage))
    return model

def scale_kpts(kpts, image_shape):
    height, width, _ = image_shape
    center = (width//2, height//2)
    scaling = 0.7*np.max([height, width])
    return np.array(kpts*scaling+center).astype(int)

def plot_matches(kpts0, kpts1, matches, img0, img1):
    imgs_shape = img0.shape
    print('shape ', imgs_shape)
    kpts0 = kpts0.detach().cpu().numpy()
    kpts1 = kpts1.detach().cpu().numpy()
    kpts0 = scale_kpts(kpts0, imgs_shape)
    kpts1 = scale_kpts(kpts1, imgs_shape)

    for kpt in kpts0:
        cv2.circle(img0, (kpt[0], kpt[1]), 3, (0, 0, 255), 3)
    for kpt in kpts1:
        cv2.circle(img1, (kpt[0], kpt[1]), 3, (0, 0, 255), 3)
    #cv2.imwrite('keypoints-'+str(pair_name[0])+'.png', img0)
    #cv2.imwrite('keypoints-'+str(pair_name[1])+'.png', img1)
    matching_img = np.concatenate((img0, img1), axis=1)
            
    for i, j in zip(range(len(matches)), matches):
        cv2.line(matching_img, (kpts0[i][0], kpts0[i][1]), 
                                (imgs_shape[1] + kpts1[j][0], kpts1[j][1]), 
                                (255, 255, 255), 1)

    cv2.imwrite('matching_'+str(pair_name[0])+'-'+str(pair_name[1])+'.png', matching_img)


if __name__ == '__main__':
    sequence = '04'
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    data_path = '/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/'
    it = get_iterator(data_path, cycle_every=None, batch_size=2,
        max_kpts=500, sequences_names=[sequence], max_skip=1, random_seed=42, get_names=True)
    
    model = ModelViz()
    model.load_state_dict(torch.load('models/weights/ego-motion-learner.pth', map_location=lambda storage, loc: storage))
    model.to(device)
    #model.eval()

    for x, y, names in it.iterate():
        _, softs = model(torch.from_numpy(x[0][:, 0, ...]).to(device), 
                          torch.from_numpy(x[0][:, 1, ...]).to(device), 
                          torch.from_numpy(x[1]).to(device))
        for kpts, scores, pair_name, soft in zip(x[0], x[1], names, softs):
            kpts0 = torch.from_numpy(kpts[0, ...])
            kpts1 = torch.from_numpy(kpts[1, ...])
            scores = torch.from_numpy(scores)
            scores = torch.abs(scores)
            mkpts0 = torch.argmax(scores, 1).detach().cpu().numpy() 
            img0_path = os.path.join(data_path+'/sequences/', sequence, 'image_2', '%06d'%pair_name[0]+'.png')
            img0 = cv2.imread(img0_path, cv2.IMREAD_COLOR)
            img0 = cv2.resize(img0, (640, 480))
            img1_path = os.path.join(data_path+'/sequences/', sequence, 'image_2', '%06d'%pair_name[1]+'.png')
            img1 = cv2.imread(img1_path, cv2.IMREAD_COLOR)
            img1 = cv2.resize(img1, (640, 480))     
            plot_matches(kpts0, kpts1, mkpts0, img0, img1)
            mkpts0 = torch.argmax(soft, 1).detach().cpu().numpy() 
            #plot_matches(kpts0, kpts1, mkpts0, img0, img1)
            #np.savetxt('scores_'+str(pair_name[0])+'.csv', soft.detach().cpu().numpy(), delimiter=',')

        break