from copy import deepcopy
from pathlib import Path
import torch
from torch import nn
import torch.nn.functional as F
from skimage import io
from models.superglue_modified import SuperGlue

def MLP(channels: list, do_bn=True):
    """ Multi-layer perceptron """
    n = len(channels)
    layers = []
    for i in range(1, n):
        layers.append(
            nn.Conv1d(channels[i - 1], channels[i], kernel_size=1, bias=True))
        if i < (n-1):
            if do_bn:
                layers.append(nn.BatchNorm1d(channels[i]))
            layers.append(nn.ReLU())
    return nn.Sequential(*layers)

class KeypointEncoder(nn.Module):
    def __init__(self, output_dim, layers):
        super().__init__()
        self.encoder = MLP([2] + layers + [output_dim])
        nn.init.constant_(self.encoder[-1].bias, 0.0)

    def forward(self, kpts):
        return self.encoder(kpts.transpose(1, 2))

class ScoresLearner(nn.Module):
    def __init__(self):
        super(ScoresLearner, self).__init__()
        self.softmax = nn.Softmax(dim=2)
        self.conv1 = nn.Conv1d(in_channels=500, out_channels=2048, kernel_size=3, padding=1)
        self.conv2 = nn.Conv1d(in_channels=2048, out_channels=1024, kernel_size=3, padding=1)
        self.conv3 = nn.Conv1d(in_channels=1024, out_channels=512, kernel_size=3, padding=1) 
        self.conv4 = nn.Conv1d(in_channels=512, out_channels=256, kernel_size=3, padding=1)
        #self.conv5 = nn.Conv1d(in_channels=256, out_channels=128, kernel_size=3, padding=1) 
        #self.conv6 = nn.Conv1d(in_channels=128, out_channels=64, kernel_size=3, padding=1) 
        #self.conv7 = nn.Conv1d(in_channels=64, out_channels=32, kernel_size=3, padding=1) 
        #self.conv8 = nn.Conv1d(in_channels=32, out_channels=2, kernel_size=3, padding=1) 

        self.relu = nn.ReLU()
        
    def forward(self, matching_scores):
        x = self.softmax(matching_scores)
        x = self.conv1(x)
        x = self.relu(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.conv4(x)
        x = self.relu(x)
        #x = self.conv5(x)
        #x = self.relu(x)
        #x = self.conv6(x)
        #x = self.relu(x)
        #x = self.conv7(x)
        #x = self.relu(x)
        #x = self.conv8(x)
        #x = self.relu(x)
        return x

class OutputPose(nn.Module):
    def __init__(self):
        super(OutputPose, self).__init__()
        self.scores = ScoresLearner()
        self.kenc =  KeypointEncoder(256, [32, 64, 128, 256])
        self.fc1 = nn.Linear(2*500*256, 1024)
        self.bn1 = nn.BatchNorm1d(1024)
        self.relu = nn.ReLU()
        self.fc2 = nn.Linear(1024, 512)
        self.bn2 = nn.BatchNorm1d(512)
        self.fc3 = nn.Linear(512, 256)
        self.bn3 = nn.BatchNorm1d(256)
        self.fc4 = nn.Linear(256, 128)
        self.bn4 = nn.BatchNorm1d(128) 
        self.fc5 = nn.Linear(128, 64)
        self.bn5 = nn.BatchNorm1d(64)
        self.fc6 = nn.Linear(64, 32)
        self.bn6 = nn.BatchNorm1d(32) 
        self.out = nn.Linear(32, 6)


    def forward(self, kpts0, kpts1, matching_scores):
        
        scores = self.scores(matching_scores)
        kpts0 = self.kenc(kpts0)  
        kpts1 = self.kenc(kpts1)
        x0 = kpts0 + scores
        x1 = kpts1 + scores
        #w0 = scores[:, 0].unsqueeze(-1)
        #w1 = scores[:, 1].unsqueeze(-1)
        #x0 = kpts0 * w0
        #x1 = kpts1 * w1
        x = torch.cat((x0, x1), 2)
        x = x.view(-1, 2*500*256)
        x = self.fc1(x)
        x = self.bn1(x)
        x = self.relu(x)

        x = self.fc2(x)
        x = self.bn2(x)
        x = self.relu(x)

        x = self.fc3(x)
        x = self.bn3(x)
        x = self.relu(x)

        x = self.fc4(x)
        x = self.bn4(x)
        x = self.relu(x)

        x = self.fc5(x)
        x = self.bn5(x)
        x = self.relu(x)

        x = self.fc6(x)
        x = self.bn6(x)
        x = self.relu(x)
        
        x = self.out(x)
        
        return x
   
if __name__ == '__main__':
    import time
    import numpy as np
    from dataset import get_iterator
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    it = get_iterator('/home/hudson/Desktop/Unicamp/Doutorado/Projeto/datasets/kitti/', cycle_every=None, batch_size=2,
        max_kpts=500, sequences_names=['03'], max_skip=1, random_seed=42)
    for x, y in it.iterate():    
        # (N, C, H, W)
        kpts = x[0]
        kpts0 = torch.from_numpy(kpts[:, 0, ...]).to(device)
        kpts1 = torch.from_numpy(kpts[:, 1, ...]).to(device)
        scores = torch.from_numpy(x[1]).to(device)
        model = OutputPose()
        model.to(device)
        init = time.time()
        # A full forward pass
        x = model(kpts0, kpts1, scores)
        print("out ", x.shape)
        print(time.time() - init)
        break