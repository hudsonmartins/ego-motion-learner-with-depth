import os
import glob
import torch
import numpy as np
import pandas as pd
from dataset import get_iterator
from model import OutputPose
from convert import prediction_to_kitti

def scaled_to_real_value(scaled_value, means, stds):
    return (scaled_value*stds)+means

def test(test_dir, sequences, model_path, output_path, random_seed):
    batch_size = 1
    #n_frames = 801
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    #scale_means = [-0.00136947, -0.0039777, 0.13808105, -0.00096686, 0.00057588, 0.00129067] 
    #scale_stds = [0.60572189, 0.03495909, 0.75906947, 0.05553685, 0.00953541, 0.0555508]
    scale_means = [4.85110811e-03, -3.37198710e-03, 1.26202869e-01, -4.70933010e-06, -6.22143044e-05, -3.87087658e-06]  
    scale_stds = [0.61432307, 0.04348949, 0.74396069, 0.4295715, 0.01735429, 0.47749098]

    model = OutputPose()
    model.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
    print("Loaded weights from ", model_path)
    model.to(device)
    model.eval()
    
    for seq in sequences:
        n_frames = len(os.listdir(os.path.join(test_dir, 'sequences/'+seq+'/image_2/')))
        test_iterator = get_iterator(test_dir, max_skip=1, max_kpts=500, cycle_every=n_frames,
            batch_size=batch_size, sequences_names=[seq], randomize=False, means=scale_means, 
            stds=scale_stds, random_seed=random_seed)
        rows = []
        row_count = 0
        with torch.no_grad():
            for x, y in test_iterator.iterate():
                kpts = x[0]
                kpts0 = torch.from_numpy(kpts[:, 0, ...]).to(device)
                kpts1 = torch.from_numpy(kpts[:, 1, ...]).to(device)
                scores = torch.from_numpy(x[1]).to(device)
                pred = model(kpts0.float(), kpts1.float(), scores.float())
                y = torch.Tensor(y).to(device)
                pred = pred.cpu().numpy()[0]
                y = y.cpu().numpy()[0]
               
                print("pred ", pred)
                print("true ", y)
                #error = criterion(pred, y)

                rows.append({'pred_x': pred[0],
                            'pred_y': pred[1],
                            'pred_z': pred[2],
                            'pred_alpha': pred[3],
                            'pred_beta': pred[4],
                            'pred_gamma': pred[5],
                            'true_x': y[0],
                            'true_y': y[1],
                            'true_z': y[2],
                            'true_alpha': y[3],
                            'true_beta': y[4],
                            'true_gamma': y[5]})
                row_count+=1
                if(row_count == n_frames):
                    break

        df = pd.DataFrame(rows)
        df.to_csv(output_path+'/predictions_'+seq+'.csv')

        print('saving trajectory')
        traj_rows = []
        traj = np.asarray([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        for i, row in df.iterrows():
            pred = np.array([row['pred_x'], row['pred_y'], row['pred_z'], 
                            row['pred_alpha'], row['pred_beta'], row['pred_gamma']])
            pred = scaled_to_real_value(pred, scale_means, scale_stds)
            traj += np.asarray(pred)
            new_row = prediction_to_kitti(traj).flatten()

            traj_rows.append({'a': new_row[0], 'b': new_row[1], 'c': new_row[2],
                            'd': new_row[3], 'e': new_row[4], 'f': new_row[5],
                            'g': new_row[6], 'h': new_row[7], 'i': new_row[8],
                            'j': new_row[9], 'k': new_row[10], 'l': new_row[11]})
        
        traj_df = pd.DataFrame(traj_rows)
        traj_df.to_csv(output_path+'/trajectory_'+seq+'.csv', header=False, index=False, sep=' ')

        print('saving true trajectory')
        traj_rows = []
        traj = np.asarray([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        for i, row in df.iterrows():
            pred = np.array([row['true_x'], row['true_y'], row['true_z'], 
                            row['true_alpha'], row['true_beta'], row['true_gamma']])
            pred = scaled_to_real_value(pred, scale_means, scale_stds)
            traj += np.asarray(pred)
            new_row = prediction_to_kitti(traj).flatten()

            traj_rows.append({'a': new_row[0], 'b': new_row[1], 'c': new_row[2],
                            'd': new_row[3], 'e': new_row[4], 'f': new_row[5],
                            'g': new_row[6], 'h': new_row[7], 'i': new_row[8],
                            'j': new_row[9], 'k': new_row[10], 'l': new_row[11]})

        traj_df = pd.DataFrame(traj_rows)
        traj_df.to_csv(output_path+'/gt_'+seq+'.csv', header=False, index=False, sep=' ')

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("test_path", default=None, help="Path to test data")
    parser.add_argument("model_path", default=None)
    parser.add_argument("--output_path", default="predictions/")
    parser.add_argument("--random_seed", default=42, type=int)


    args = parser.parse_args()
    if not os.path.exists(args.output_path):
        os.mkdir(args.output_path)

    sequences=['04', '06', '08', '09']
    test(args.test_path, sequences, args.model_path, args.output_path, args.random_seed)
